const PostModel = require("../models/postModel").PostModel;
const jwt = require("jsonwebtoken");

module.exports = {
  insertPost: async (req, res) => {
    const { title, content } = req.body;
    const post = new PostModel({
      title: req.body.title,
      content: req.body.content
    });
    post.save();
    res.json(post);
  },
  getPosts: async (req, res) => {
    var pageNo = req.params.pageNo;
    PostModel.paginate({}, { page: pageNo, limit: 10 }, function(err, result) {
      res.json(result);
    });
  }
};
