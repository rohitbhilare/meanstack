const express = require("express");
const router = new express.Router();

const PostController = require("../controllers/postController");
const UserController = require("../controllers/userController");
const auth = require("../middleware/authenticate");
router.post("/api/post", auth, PostController.insertPost);
router.get("/api/posts/:pageNo", auth, PostController.getPosts);
router.post("/api/login", UserController.AuthenticateUser);
router.post("/api/register", UserController.creatUser);

module.exports = router;
