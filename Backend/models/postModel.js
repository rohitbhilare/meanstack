const mongoose = require("mongoose");
var mongoosePaginate = require('mongoose-paginate');
const schema = mongoose.Schema;

const PostSchema = schema({
  title: { type: String, require: true },
  content: { type: String, require: true }
});

PostSchema.plugin(mongoosePaginate);
var postSchema = mongoose.model("Post", PostSchema);

module.exports = { PostModel: postSchema };
