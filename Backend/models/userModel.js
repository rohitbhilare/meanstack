const mongoose = require("mongoose");
const schema = mongoose.Schema;

const UserSchema = schema({
  username: { type: String, require: true, unique: true },
  password: { type: String, require: true },
  email: { type: String, require: true, unique: true },
  mobile: { type: String, require: true }
});

const UserModel = mongoose.model("userModel", UserSchema);

module.exports = { userModel: UserModel };
