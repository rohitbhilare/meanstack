import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel, UserReg } from './login.model';

export class LoginService {
    constructor(private http: HttpClient) { }
    private token: any;

    // getToken() {
    //     console.log(this.token);
    //     return this.token.token;
    // }

    LoginUser(loginModel) {
        this.http.post("http://localhost:3000/api/login", loginModel).subscribe(
            token => {
                console.log(token);
                this.token = token;
                window.localStorage.setItem('token', this.token.token)
            }
        )
    }
    RegisterUser() {
        this.http.post("http://localhost:3000/api/register", UserReg).subscribe(
            register => {
                console.log(register);

            }
        )
    }
}