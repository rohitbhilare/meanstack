import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserModel, UserReg } from '../login.model';
import * as bcrypt from 'bcryptjs';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  passwordhide: boolean = true;
  cnfpasswordhide: boolean = true;
  regpasswordhide: boolean = true;
  passwordInput: any;
  loginForm: NgForm;
  confirmPassword: string;

  userModel: UserModel = new UserModel();
  userReg: UserReg = new UserReg();
  Err: boolean;
  constructor() { }

  login() {
    const salt = bcrypt.genSaltSync(10);
    var pass = bcrypt.hashSync('Pass@123', 10);
    console.log(pass);
  }

  checkConfirmPassword() {
    if (this.userReg.password === this.confirmPassword) {
      this.Err = false;
    } else {
      this.Err = true;
    }
  }
  register() {
    console.log(this.userReg);

  }

  ngOnInit() {
  }

}
