import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from "./User/login/login.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private loginService: LoginService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('called');
        let authToken = '';
        try {
            authToken = window.localStorage.getItem('token');
        } catch (error) {

        }
        const authrequest = req.clone({ headers: req.headers.set("Authorization", "Bearer " + authToken) });
        // const authReq = 
        return next.handle(authrequest);

    }
}