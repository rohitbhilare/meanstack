import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Post } from '../post.model';
import { PostService } from '../post.service';
import { PostListComponent } from "../post-list/post-list.component";
import { LoginService } from '../../User/login/login.service';

@Component({
    selector: 'app-createpost',
    templateUrl: './createpost.component.html',
    styleUrls: ['./createpost.component.css'],
    providers :[LoginService,PostService]
})


export class CreatepostComponent {
    // Title: string;
    // Post: string;
    Posts: Post = new Post();

    @Output() postCreated: EventEmitter<Post> = new EventEmitter();

    constructor(private postService: PostService) { }
    addPost(postForm: NgForm) {
        // let PostList = new PostListComponent(this.postService);
        if (!postForm.form.valid) {
            return false;
        }
        console.log(this.Posts);
        this.postService.addPost(this.Posts).subscribe(
            data => {
                console.log(data);
                this.postCreated.emit(data);
            }
        )

    }
}
