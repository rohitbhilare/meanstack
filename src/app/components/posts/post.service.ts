import { Post } from './post.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

export class PostService {
    posts: Post = new Post();
    constructor(private http: HttpClient) { }


    getallPost(pageNo: string) {
        return this.http.get<any>('http://localhost:3000/api/posts/' + pageNo);
    }

    addPost(posts: Post): Observable<Post> {
        let httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
                // ,'Access-Control-Allow-Origin': '*'
            })
        };
        return this.http.post<Post>('http://localhost:3000/api/post', posts, httpOptions).pipe(

        )
    }
}
