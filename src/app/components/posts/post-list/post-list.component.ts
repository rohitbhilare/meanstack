import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../post.service';
import { PageEvent } from '@angular/material/paginator';
import { UserModel } from '../../User/login/login.model';
import { LoginService } from '../../User/login/login.service';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  totalRecord: any;
  pageEvent: PageEvent;
  loginModel: UserModel = new UserModel();

  constructor(private postService: PostService, private UserLoginService: LoginService) { }

  @Input('posts') posts = [];


  getallservice(page) {
    console.log(page);
    this.postService.getallPost(page).subscribe(
      data => {
        this.posts = data.docs;
        this.totalRecord = data.total;
        console.log(this.posts);
      });
  }

  postCheck(evt) {
    console.log(evt);

  }

  UserLogin() {
    this.loginModel.userName = 'rohitbhilare03';
    this.loginModel.password = 'Roh.bhi03';
    this.UserLoginService.LoginUser(this.loginModel)

  }



  onPaginateChange(event) {
    this.getallservice(event.pageIndex + 1)
  }

  ngOnInit() {
    this.getallservice(1);
    // this.PostService.getallPost().subscribe();
    console.log(this.posts);
  }

}
