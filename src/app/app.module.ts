import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreatepostComponent } from './components/posts/post-create/createpost.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from 'src/shared-module/material/material.module';
import { FormsModule } from '@angular/forms';
import { PostListComponent } from './components/posts/post-list/post-list.component';
import { PostService } from './components/posts/post.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatPaginatorModule } from '@angular/material';
import { LoginService } from './components/User/login/login.service';
import { AuthInterceptor } from "./components/auth-interceptor";
import { LoginComponent } from './components/User/login/UserLogin/login.component';
import { MainComponent } from './components/main/main.component';

@NgModule({
  declarations: [
    AppComponent,
    CreatepostComponent,
    HeaderComponent,
    PostListComponent,
    LoginComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule,
    HttpClientModule,
    MatPaginatorModule
  ],
  providers: [PostService, LoginService, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
