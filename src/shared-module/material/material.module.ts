import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule, MatIconModule, MatFormFieldModule, MatInputModule, MatExpansionModule, MatButtonModule } from '@angular/material';


@NgModule({
  declarations: [],
  exports: [
    CommonModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatButtonModule
  ]
})
export class MaterialModule { }
